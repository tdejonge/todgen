---
user:
  name: Thomas De Jonge
  job: Full Stack Software Engineer
  header:
    expertise:
      - Java programming
    languages:
      - Dutch
      - English
      - French
    location: Haaltert
    mobility:
      - Region East Flanders
  projects:
    - name: Web Applications Traineeship
      start: September 2019
      end: November 2019
      company: Nalys
      location: Brussels 
      context: "\\textbf{Nalys} is a consulting group dedicated to high-technology projects. In order to create better than regular junior engineers, the Nalys Institute of Technology (NIT) organizes the \\textbf{Traineeship}:  a hands-on industrial grade training program. During the first 5 weeks of the program, the engineers are taught in hard and soft skills. This training is organized in theory and lab sessions, with an emphasis on practical workshops. It teaches the engineers to work under pressure and at a high pace. After the first 5 weeks of trainings, a 4-week \\textbf{Traineeship Project} is organized in which a real-life problem is solved together with a client. After the traineeship, the engineers are coached by experienced Nalys engineers."
      context_short: Traineeship as preparation in Web Applications.
      achievements: 
        - "Linux, Shell scripting, Vim, Git"
        - "TCP/IP and Networking"
        - "Back-end development: Java 8, Spring, Maven, JDBC, REST APIs"
        - "Front-end development: Javascript, Angular"
        - "Web and Cloud development: AWS, Docker"
        - "Test Driven Development"
        - "Professional focus: Code quality, Review process, Professional documentation, Communication, Eye for detail"
        - "Critical Thinking: Problem solving, Root cause analysis, Autonomy, Efficient working"
      achievements_short: 
        - "Linux, Networking, Vim, Git, TDD, docker"
        - "Java 8, Spring, REST APIs, Javascript, Angular"
    - name: Thesis
      start: September 2018
      end: June 2019
      company: Ghent University
      location: Ghent
      context: "The PATRONUS consortium brings together experts from various fields such as exposure therapy, hard- and software for data analysis, scalable dataprocessing and virtual reality, as well as patient-oriented services. The goal of the project is to develop an effective and personalised solution consisting of IT-tools that guide the therapist and patient during therapy. This thesis is conducted within the scope of the PATRONUS project, and realised an application that provides personalised and adaptive relaxation therapy using virtual reality."
      context_short: Personalised Virtual Reality in Relaxation Therapy.
      achievements:
        - "\\textbf{Java(FX) programming}: Creating a GUI used by the patient to fill in a questionnaire and by the therapist to guide and observe the therapy. This project used Maven."
        - "\\textbf{Unity development}: VR scene development, C\\# programming"
        - "\\textbf{Python scripting}: Processing, evaluation, and visualisation of the results."
        - "\\textbf{TCP/IP}: Connecting the Java (server) and Unity (client) applications using TCP/IP."
        - "\\textbf{Modelling human behaviour}: Simulating feelings and emotions"
        - "\\textbf{Soft skills}: Consulting with experts from the mental healthcare sector to continuously update the software design, presenting the project, receiving and processing feedback."
        - "\\textbf{Published demo paper}: Joris Heyse, Thomas De Jonge, Maria Torres Vega, Femke De Backere, and Filip De Turck. ``A personalized Virtual Reality Experience for Relaxation Therapy.`` In 2020 Eleventh International Conference on Quality of Multimedia Experience (QoMEX), pp. 1-3. IEEE, 2019."
      achievements_short:
        - "Java(FX), Unity, C\\#, Python"
        - "Human emotions modelling"
    - name: Internship
      start: July 2018
      end: August 2018
      company: Ikanda
      location: Ninove
      context: "IKANDA buils digital experiences for smart buildings using sensor technologies and digital signage solutions. For this internship, IKANDA wanted to use NFC technology to control content displayed on a TV screen. The screens were controlled by an Intel NUC, to which the NFC sensors were attached. Two applications were realised: (1) a GUI to easily program the NFC tags, and (2) an application that controls the displayed content, it connects to NFC sensors and changes the content based on the tag(s) detected."
      context_short: Using NFC technology to configure content on a screen 
      achievements:
        - "\\textbf{C programming}: Extending the NFC library to connect multiple NFC readers to the intel NUC (multi threaded) and allow for easily programming of NFC tags"
        - "\\textbf{C++ programming}: Creation of a GUI to easily program NFC tags and an application that configures the displayed content." 
        - "\\textbf{Soft skills}: presenting and discussing the product"
      achievements_short:
        - "C and C++ programming"
        - "NFC technology"
    - name: Design Project
      start: September 2016
      end: June 2017
      company: Ghent University
      location: Ghent
      context: "The PATRONUS consortium brings together experts from various fields such as exposure therapy, hard- and software for data analysis, scalable dataprocessing and virtual reality, as well as patient-oriented services. The goal of the project is to develop an effective and personalised solution consisting of IT-tools that guide the therapist and patient during therapy. This Design Project was conducted within the scope of the PATRONUS project. The goal was to create an application that helps people suffering from acrophobia (fear of heights) using SCRUM methodology."
      context_short: Virtual Reality for acrophobia (fear of heights) therapy.
      achievements:
          - "\\textbf{Project management and project methodology}: SCRUM"
          - "\\textbf{Design}: Technical design of hardware and software, prototype design"
          - "\\textbf{Results}: Documentation, cost budget/risk-analysis, and evaluation of the final results."
          - "\\textbf{Unity development}: VR scene development, C\\# programming"
          - "\\textbf{Android development}: Design and creation of an Empatica E4 plugin (in Java). This plugin was imported into Unity and allowed for communication between Unity and the Empatica E4."
          - "\\textbf{Soft skills}: Working in team (8 people), presenting, exchanging feedback and decision making"
      achievements_short:
          - "Unity and Android development"
          - "C\\# and Java programming"
          - "SCRUM methodology"
  skills:
    - name: Java programming
      score: 3
    - name: "C++ programming"
      score: 2
    - name: "C\\# programming"
      score: 2
    - name: Git
      score: 2
    - name: LaTeX
      score: 2
    - name: Linux
      score: 2
    - name: Python
      score: 2
    - name: Networking
      score: 2
    - name: Agile methodology
      score: 2
    - name: Unity development
      score: 2
    - name: Android development
      score: 1
    - name: C programming
      score: 1
    - name: Shell
      score: 1

  studies: "Master of Science in Computer Science Engineering at Ghent University, graduated in 2019"
