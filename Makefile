.PHONY:dockerimage

dockerimage:
	(cd docker && docker build -t todgen_image .)


%.pdf: %/ dockerimage
	touch $@
	mkdir cache -p
	docker run --rm -v $(shell pwd)/cache/:/usr/local/cargo/registry/ -v $(shell pwd)/todgen/target/:/app/todgen/target --mount type=bind,source=$(shell pwd)/$@,target=/app/data.pdf --mount type=bind,source=$(shell pwd)/$<,target=/app/data/,readonly -v $(shell pwd)/todgen:/app/todgen -w /app todgen_image make -C todgen/ 

